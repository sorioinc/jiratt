﻿namespace Jira2TT
{
    partial class xfrmJira
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tbJiraUser = new DevExpress.XtraEditors.TextEdit();
            this.tbJiraPassword = new DevExpress.XtraEditors.TextEdit();
            this.btJiraLogin = new DevExpress.XtraEditors.SimpleButton();
            this.btGetIssues = new DevExpress.XtraEditors.SimpleButton();
            this.gcWork = new DevExpress.XtraGrid.GridControl();
            this.workLogBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIssue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeSpentMin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dteFrom = new DevExpress.XtraEditors.DateEdit();
            this.dteTo = new DevExpress.XtraEditors.DateEdit();
            this.lblTotalWage = new DevExpress.XtraEditors.GroupControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.tbFilterUser = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.tbURL = new DevExpress.XtraEditors.TextEdit();
            this.lbProjects = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl100 = new DevExpress.XtraEditors.LabelControl();
            this.seIssuesRetrieve = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.tbDescription = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.tbMeetingTicket = new DevExpress.XtraEditors.TextEdit();
            this.btPopulate = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.seMeetingDuration = new DevExpress.XtraEditors.SpinEdit();
            this.teMeetingStartTime = new DevExpress.XtraEditors.TimeEdit();
            this.pbBusy = new DevExpress.XtraEditors.ProgressBarControl();
            this.bwGetJSON = new System.ComponentModel.BackgroundWorker();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.cbClients = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.btPostTimeTrack = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.luePayPeriod = new DevExpress.XtraEditors.LookUpEdit();
            this.tbTTUser = new DevExpress.XtraEditors.TextEdit();
            this.tbTTPassword = new DevExpress.XtraEditors.TextEdit();
            this.btTTLogin = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.lblCurrentIncome = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.seWageHour = new DevExpress.XtraEditors.SpinEdit();
            this.lblTotalWagePeriod = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.lblHoursThrough = new DevExpress.XtraEditors.LabelControl();
            this.lblHoursLogged = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.lblTotalHoursPeriod = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            ((System.ComponentModel.ISupportInitialize)(this.tbJiraUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbJiraPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcWork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workLogBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFrom.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteTo.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalWage)).BeginInit();
            this.lblTotalWage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbFilterUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbURL.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbProjects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seIssuesRetrieve.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMeetingTicket.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seMeetingDuration.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teMeetingStartTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBusy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbClients.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePayPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTTUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTTPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seWageHour.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbJiraUser
            // 
            this.tbJiraUser.Location = new System.Drawing.Point(23, 94);
            this.tbJiraUser.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbJiraUser.Name = "tbJiraUser";
            this.tbJiraUser.Properties.NullValuePrompt = "User";
            this.tbJiraUser.Properties.NullValuePromptShowForEmptyValue = true;
            this.tbJiraUser.Size = new System.Drawing.Size(117, 20);
            this.tbJiraUser.TabIndex = 0;
            // 
            // tbJiraPassword
            // 
            this.tbJiraPassword.Location = new System.Drawing.Point(23, 126);
            this.tbJiraPassword.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbJiraPassword.Name = "tbJiraPassword";
            this.tbJiraPassword.Properties.NullValuePrompt = "Password";
            this.tbJiraPassword.Properties.NullValuePromptShowForEmptyValue = true;
            this.tbJiraPassword.Properties.PasswordChar = '•';
            this.tbJiraPassword.Size = new System.Drawing.Size(117, 20);
            this.tbJiraPassword.TabIndex = 1;
            // 
            // btJiraLogin
            // 
            this.btJiraLogin.Location = new System.Drawing.Point(147, 108);
            this.btJiraLogin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btJiraLogin.Name = "btJiraLogin";
            this.btJiraLogin.Size = new System.Drawing.Size(64, 28);
            this.btJiraLogin.TabIndex = 2;
            this.btJiraLogin.Text = "Login";
            this.btJiraLogin.Click += new System.EventHandler(this.btJiraLogin_Click);
            // 
            // btGetIssues
            // 
            this.btGetIssues.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btGetIssues.Enabled = false;
            this.btGetIssues.Location = new System.Drawing.Point(763, 132);
            this.btGetIssues.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btGetIssues.Name = "btGetIssues";
            this.btGetIssues.Size = new System.Drawing.Size(87, 28);
            this.btGetIssues.TabIndex = 6;
            this.btGetIssues.Text = "Get Issues";
            this.btGetIssues.Click += new System.EventHandler(this.btGetIssues_Click);
            // 
            // gcWork
            // 
            this.gcWork.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcWork.DataSource = this.workLogBindingSource;
            this.gcWork.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcWork.Location = new System.Drawing.Point(12, 438);
            this.gcWork.MainView = this.gridView1;
            this.gcWork.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcWork.Name = "gcWork";
            this.gcWork.Size = new System.Drawing.Size(1193, 269);
            this.gcWork.TabIndex = 4;
            this.gcWork.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // workLogBindingSource
            // 
            this.workLogBindingSource.DataSource = typeof(Jira2TT.WorkLog);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIssue,
            this.colStartTime,
            this.colTimeSpentMin,
            this.colDescription});
            this.gridView1.GridControl = this.gcWork;
            this.gridView1.GroupCount = 1;
            this.gridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TimeSpentMin", null, "")});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.ReadOnly = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartTime, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colIssue
            // 
            this.colIssue.FieldName = "Issue";
            this.colIssue.Name = "colIssue";
            this.colIssue.Visible = true;
            this.colIssue.VisibleIndex = 0;
            this.colIssue.Width = 79;
            // 
            // colStartTime
            // 
            this.colStartTime.DisplayFormat.FormatString = "G";
            this.colStartTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colStartTime.FieldName = "StartTime";
            this.colStartTime.Name = "colStartTime";
            this.colStartTime.Visible = true;
            this.colStartTime.VisibleIndex = 1;
            this.colStartTime.Width = 142;
            // 
            // colTimeSpentMin
            // 
            this.colTimeSpentMin.FieldName = "TimeSpentMin";
            this.colTimeSpentMin.Name = "colTimeSpentMin";
            this.colTimeSpentMin.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TimeSpentMin", "Minutes: {0:n0}")});
            this.colTimeSpentMin.Visible = true;
            this.colTimeSpentMin.VisibleIndex = 1;
            this.colTimeSpentMin.Width = 78;
            // 
            // colDescription
            // 
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 2;
            this.colDescription.Width = 374;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(352, 97);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(241, 97);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "From:";
            // 
            // dteFrom
            // 
            this.dteFrom.EditValue = null;
            this.dteFrom.Location = new System.Drawing.Point(241, 118);
            this.dteFrom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dteFrom.Name = "dteFrom";
            this.dteFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteFrom.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dteFrom.Size = new System.Drawing.Size(104, 20);
            this.dteFrom.TabIndex = 3;
            // 
            // dteTo
            // 
            this.dteTo.EditValue = null;
            this.dteTo.Location = new System.Drawing.Point(352, 118);
            this.dteTo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dteTo.Name = "dteTo";
            this.dteTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteTo.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dteTo.Size = new System.Drawing.Size(103, 20);
            this.dteTo.TabIndex = 4;
            // 
            // lblTotalWage
            // 
            this.lblTotalWage.Controls.Add(this.labelControl12);
            this.lblTotalWage.Controls.Add(this.tbFilterUser);
            this.lblTotalWage.Controls.Add(this.labelControl11);
            this.lblTotalWage.Controls.Add(this.tbURL);
            this.lblTotalWage.Controls.Add(this.lbProjects);
            this.lblTotalWage.Controls.Add(this.labelControl8);
            this.lblTotalWage.Controls.Add(this.LabelControl100);
            this.lblTotalWage.Controls.Add(this.seIssuesRetrieve);
            this.lblTotalWage.Controls.Add(this.labelControl2);
            this.lblTotalWage.Controls.Add(this.tbJiraUser);
            this.lblTotalWage.Controls.Add(this.labelControl1);
            this.lblTotalWage.Controls.Add(this.dteFrom);
            this.lblTotalWage.Controls.Add(this.btGetIssues);
            this.lblTotalWage.Controls.Add(this.tbJiraPassword);
            this.lblTotalWage.Controls.Add(this.dteTo);
            this.lblTotalWage.Controls.Add(this.btJiraLogin);
            this.lblTotalWage.Location = new System.Drawing.Point(14, 15);
            this.lblTotalWage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblTotalWage.Name = "lblTotalWage";
            this.lblTotalWage.Size = new System.Drawing.Size(884, 192);
            this.lblTotalWage.TabIndex = 6;
            this.lblTotalWage.Text = "Jira Credentials";
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(23, 165);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(53, 13);
            this.labelControl12.TabIndex = 25;
            this.labelControl12.Text = "User Filter:";
            // 
            // tbFilterUser
            // 
            this.tbFilterUser.Location = new System.Drawing.Point(92, 161);
            this.tbFilterUser.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbFilterUser.Name = "tbFilterUser";
            this.tbFilterUser.Properties.NullValuePrompt = "User";
            this.tbFilterUser.Properties.NullValuePromptShowForEmptyValue = true;
            this.tbFilterUser.Size = new System.Drawing.Size(117, 20);
            this.tbFilterUser.TabIndex = 24;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(23, 41);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(23, 13);
            this.labelControl11.TabIndex = 23;
            this.labelControl11.Text = "URL:";
            // 
            // tbURL
            // 
            this.tbURL.Location = new System.Drawing.Point(23, 62);
            this.tbURL.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbURL.Name = "tbURL";
            this.tbURL.Properties.NullValuePrompt = "URL";
            this.tbURL.Properties.NullValuePromptShowForEmptyValue = true;
            this.tbURL.Size = new System.Drawing.Size(432, 20);
            this.tbURL.TabIndex = 22;
            // 
            // lbProjects
            // 
            this.lbProjects.DisplayMember = "name";
            this.lbProjects.Location = new System.Drawing.Point(493, 65);
            this.lbProjects.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lbProjects.Name = "lbProjects";
            this.lbProjects.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lbProjects.Size = new System.Drawing.Size(195, 87);
            this.lbProjects.TabIndex = 17;
            this.lbProjects.ValueMember = "key";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(493, 44);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(43, 13);
            this.labelControl8.TabIndex = 16;
            this.labelControl8.Text = "Projects:";
            // 
            // LabelControl100
            // 
            this.LabelControl100.Location = new System.Drawing.Point(710, 53);
            this.LabelControl100.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LabelControl100.Name = "LabelControl100";
            this.LabelControl100.Size = new System.Drawing.Size(169, 13);
            this.LabelControl100.TabIndex = 14;
            this.LabelControl100.Text = "# of Issues to retrieve per project:";
            // 
            // seIssuesRetrieve
            // 
            this.seIssuesRetrieve.EditValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.seIssuesRetrieve.Location = new System.Drawing.Point(750, 74);
            this.seIssuesRetrieve.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.seIssuesRetrieve.Name = "seIssuesRetrieve";
            this.seIssuesRetrieve.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seIssuesRetrieve.Size = new System.Drawing.Size(100, 20);
            this.seIssuesRetrieve.TabIndex = 5;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(37, 137);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(57, 13);
            this.labelControl10.TabIndex = 21;
            this.labelControl10.Text = "Description:";
            // 
            // tbDescription
            // 
            this.tbDescription.EditValue = "Scrum Meeting";
            this.tbDescription.Enabled = false;
            this.tbDescription.Location = new System.Drawing.Point(129, 132);
            this.tbDescription.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Properties.NullValuePrompt = "Description to Log";
            this.tbDescription.Properties.NullValuePromptShowForEmptyValue = true;
            this.tbDescription.Size = new System.Drawing.Size(145, 20);
            this.tbDescription.TabIndex = 20;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(37, 105);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(73, 13);
            this.labelControl9.TabIndex = 19;
            this.labelControl9.Text = "Meeting Ticket:";
            // 
            // tbMeetingTicket
            // 
            this.tbMeetingTicket.Enabled = false;
            this.tbMeetingTicket.Location = new System.Drawing.Point(129, 100);
            this.tbMeetingTicket.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbMeetingTicket.Name = "tbMeetingTicket";
            this.tbMeetingTicket.Properties.NullValuePrompt = "Ticket Number";
            this.tbMeetingTicket.Properties.NullValuePromptShowForEmptyValue = true;
            this.tbMeetingTicket.Size = new System.Drawing.Size(145, 20);
            this.tbMeetingTicket.TabIndex = 18;
            // 
            // btPopulate
            // 
            this.btPopulate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btPopulate.Enabled = false;
            this.btPopulate.Location = new System.Drawing.Point(37, 162);
            this.btPopulate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btPopulate.Name = "btPopulate";
            this.btPopulate.Size = new System.Drawing.Size(237, 28);
            this.btPopulate.TabIndex = 15;
            this.btPopulate.Text = "Post Meetings in Jira";
            this.btPopulate.Click += new System.EventHandler(this.btPopulate_Click);
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(174, 45);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(86, 13);
            this.labelControl5.TabIndex = 14;
            this.labelControl5.Text = "Meeting Duration:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(37, 45);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(94, 13);
            this.labelControl4.TabIndex = 13;
            this.labelControl4.Text = "Meeting Start Time:";
            // 
            // seMeetingDuration
            // 
            this.seMeetingDuration.EditValue = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.seMeetingDuration.Enabled = false;
            this.seMeetingDuration.Location = new System.Drawing.Point(174, 68);
            this.seMeetingDuration.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.seMeetingDuration.Name = "seMeetingDuration";
            this.seMeetingDuration.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seMeetingDuration.Size = new System.Drawing.Size(100, 20);
            this.seMeetingDuration.TabIndex = 12;
            // 
            // teMeetingStartTime
            // 
            this.teMeetingStartTime.EditValue = new System.DateTime(2013, 2, 14, 0, 0, 0, 0);
            this.teMeetingStartTime.Enabled = false;
            this.teMeetingStartTime.Location = new System.Drawing.Point(37, 68);
            this.teMeetingStartTime.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.teMeetingStartTime.Name = "teMeetingStartTime";
            this.teMeetingStartTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.teMeetingStartTime.Size = new System.Drawing.Size(117, 20);
            this.teMeetingStartTime.TabIndex = 11;
            // 
            // pbBusy
            // 
            this.pbBusy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbBusy.Location = new System.Drawing.Point(14, 715);
            this.pbBusy.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbBusy.Name = "pbBusy";
            this.pbBusy.Size = new System.Drawing.Size(1191, 22);
            this.pbBusy.TabIndex = 7;
            this.pbBusy.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.pbBusy_CustomDisplayText);
            // 
            // bwGetJSON
            // 
            this.bwGetJSON.WorkerReportsProgress = true;
            this.bwGetJSON.WorkerSupportsCancellation = true;
            this.bwGetJSON.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwGetJSON_DoWork);
            this.bwGetJSON.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwGetJSON_ProgressChanged);
            this.bwGetJSON.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwGetJSON_RunWorkerCompleted);
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.cbClients);
            this.groupControl3.Controls.Add(this.labelControl7);
            this.groupControl3.Controls.Add(this.btPostTimeTrack);
            this.groupControl3.Controls.Add(this.labelControl3);
            this.groupControl3.Controls.Add(this.luePayPeriod);
            this.groupControl3.Controls.Add(this.tbTTUser);
            this.groupControl3.Controls.Add(this.tbTTPassword);
            this.groupControl3.Controls.Add(this.btTTLogin);
            this.groupControl3.Location = new System.Drawing.Point(14, 214);
            this.groupControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(884, 112);
            this.groupControl3.TabIndex = 8;
            this.groupControl3.Text = "TimeTrack Credentials";
            // 
            // cbClients
            // 
            this.cbClients.Location = new System.Drawing.Point(436, 57);
            this.cbClients.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbClients.Name = "cbClients";
            this.cbClients.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbClients.Properties.Items.AddRange(new object[] {
            "Trieagle",
            "Prosoft Nearshore",
            "One Legal"});
            this.cbClients.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbClients.Size = new System.Drawing.Size(194, 20);
            this.cbClients.TabIndex = 18;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(436, 33);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(31, 13);
            this.labelControl7.TabIndex = 17;
            this.labelControl7.Text = "Client:";
            // 
            // btPostTimeTrack
            // 
            this.btPostTimeTrack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btPostTimeTrack.Enabled = false;
            this.btPostTimeTrack.Location = new System.Drawing.Point(766, 54);
            this.btPostTimeTrack.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btPostTimeTrack.Name = "btPostTimeTrack";
            this.btPostTimeTrack.Size = new System.Drawing.Size(87, 28);
            this.btPostTimeTrack.TabIndex = 9;
            this.btPostTimeTrack.Text = "Post";
            this.btPostTimeTrack.Click += new System.EventHandler(this.btPostTimeTrack_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(236, 33);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(55, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Pay Period:";
            // 
            // luePayPeriod
            // 
            this.luePayPeriod.Location = new System.Drawing.Point(236, 57);
            this.luePayPeriod.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.luePayPeriod.Name = "luePayPeriod";
            this.luePayPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luePayPeriod.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DisplayMember", "Pay Period")});
            this.luePayPeriod.Properties.DisplayMember = "DisplayMember";
            this.luePayPeriod.Properties.NullText = "[Login to get Pay Periods]";
            this.luePayPeriod.Properties.ValueMember = "ValueMember";
            this.luePayPeriod.Size = new System.Drawing.Size(194, 20);
            this.luePayPeriod.TabIndex = 3;
            // 
            // tbTTUser
            // 
            this.tbTTUser.Location = new System.Drawing.Point(23, 39);
            this.tbTTUser.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbTTUser.Name = "tbTTUser";
            this.tbTTUser.Properties.NullValuePrompt = "User";
            this.tbTTUser.Properties.NullValuePromptShowForEmptyValue = true;
            this.tbTTUser.Size = new System.Drawing.Size(117, 20);
            this.tbTTUser.TabIndex = 0;
            // 
            // tbTTPassword
            // 
            this.tbTTPassword.Location = new System.Drawing.Point(23, 71);
            this.tbTTPassword.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbTTPassword.Name = "tbTTPassword";
            this.tbTTPassword.Properties.NullValuePrompt = "Password";
            this.tbTTPassword.Properties.NullValuePromptShowForEmptyValue = true;
            this.tbTTPassword.Properties.PasswordChar = '•';
            this.tbTTPassword.Size = new System.Drawing.Size(117, 20);
            this.tbTTPassword.TabIndex = 1;
            // 
            // btTTLogin
            // 
            this.btTTLogin.Location = new System.Drawing.Point(147, 54);
            this.btTTLogin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btTTLogin.Name = "btTTLogin";
            this.btTTLogin.Size = new System.Drawing.Size(64, 28);
            this.btTTLogin.TabIndex = 2;
            this.btTTLogin.Text = "Login";
            this.btTTLogin.Click += new System.EventHandler(this.btTTLogin_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.lblCurrentIncome);
            this.groupControl1.Controls.Add(this.labelControl18);
            this.groupControl1.Controls.Add(this.labelControl16);
            this.groupControl1.Controls.Add(this.seWageHour);
            this.groupControl1.Controls.Add(this.lblTotalWagePeriod);
            this.groupControl1.Controls.Add(this.labelControl15);
            this.groupControl1.Controls.Add(this.labelControl13);
            this.groupControl1.Controls.Add(this.lblHoursThrough);
            this.groupControl1.Controls.Add(this.lblHoursLogged);
            this.groupControl1.Controls.Add(this.labelControl17);
            this.groupControl1.Controls.Add(this.lblTotalHoursPeriod);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Location = new System.Drawing.Point(14, 334);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(884, 96);
            this.groupControl1.TabIndex = 9;
            this.groupControl1.Text = "Summary";
            // 
            // lblCurrentIncome
            // 
            this.lblCurrentIncome.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.lblCurrentIncome.Location = new System.Drawing.Point(256, 77);
            this.lblCurrentIncome.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblCurrentIncome.Name = "lblCurrentIncome";
            this.lblCurrentIncome.Size = new System.Drawing.Size(6, 13);
            this.lblCurrentIncome.TabIndex = 15;
            this.lblCurrentIncome.Text = "0";
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(157, 77);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(79, 13);
            this.labelControl18.TabIndex = 14;
            this.labelControl18.Text = "Current Income:";
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(10, 35);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(84, 13);
            this.labelControl16.TabIndex = 13;
            this.labelControl16.Text = "Income per Hour:";
            // 
            // seWageHour
            // 
            this.seWageHour.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.seWageHour.Location = new System.Drawing.Point(15, 55);
            this.seWageHour.Name = "seWageHour";
            this.seWageHour.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seWageHour.Size = new System.Drawing.Size(75, 20);
            this.seWageHour.TabIndex = 12;
            this.seWageHour.EditValueChanged += new System.EventHandler(this.seWageHour_EditValueChanged);
            // 
            // lblTotalWagePeriod
            // 
            this.lblTotalWagePeriod.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.lblTotalWagePeriod.Location = new System.Drawing.Point(682, 51);
            this.lblTotalWagePeriod.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblTotalWagePeriod.Name = "lblTotalWagePeriod";
            this.lblTotalWagePeriod.Size = new System.Drawing.Size(6, 13);
            this.lblTotalWagePeriod.TabIndex = 11;
            this.lblTotalWagePeriod.Text = "0";
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(533, 51);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(121, 13);
            this.labelControl15.TabIndex = 10;
            this.labelControl15.Text = "Possible wage for period:";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControl13.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl13.Location = new System.Drawing.Point(266, 53);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(5, 16);
            this.labelControl13.TabIndex = 9;
            this.labelControl13.Text = "/";
            // 
            // lblHoursThrough
            // 
            this.lblHoursThrough.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblHoursThrough.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.lblHoursThrough.Location = new System.Drawing.Point(291, 53);
            this.lblHoursThrough.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblHoursThrough.Name = "lblHoursThrough";
            this.lblHoursThrough.Size = new System.Drawing.Size(7, 16);
            this.lblHoursThrough.TabIndex = 8;
            this.lblHoursThrough.Text = "0";
            // 
            // lblHoursLogged
            // 
            this.lblHoursLogged.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblHoursLogged.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.lblHoursLogged.Location = new System.Drawing.Point(233, 53);
            this.lblHoursLogged.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblHoursLogged.Name = "lblHoursLogged";
            this.lblHoursLogged.Size = new System.Drawing.Size(7, 16);
            this.lblHoursLogged.TabIndex = 6;
            this.lblHoursLogged.Text = "0";
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(157, 30);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(247, 13);
            this.labelControl17.TabIndex = 5;
            this.labelControl17.Text = "Current Logged Hours / Hours through from Period:";
            // 
            // lblTotalHoursPeriod
            // 
            this.lblTotalHoursPeriod.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.lblTotalHoursPeriod.Location = new System.Drawing.Point(682, 30);
            this.lblTotalHoursPeriod.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblTotalHoursPeriod.Name = "lblTotalHoursPeriod";
            this.lblTotalHoursPeriod.Size = new System.Drawing.Size(6, 13);
            this.lblTotalHoursPeriod.TabIndex = 4;
            this.lblTotalHoursPeriod.Text = "0";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(533, 30);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(122, 13);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = "Possible hours for period:";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.labelControl4);
            this.groupControl2.Controls.Add(this.teMeetingStartTime);
            this.groupControl2.Controls.Add(this.seMeetingDuration);
            this.groupControl2.Controls.Add(this.labelControl5);
            this.groupControl2.Controls.Add(this.labelControl10);
            this.groupControl2.Controls.Add(this.btPopulate);
            this.groupControl2.Controls.Add(this.tbDescription);
            this.groupControl2.Controls.Add(this.tbMeetingTicket);
            this.groupControl2.Controls.Add(this.labelControl9);
            this.groupControl2.Enabled = false;
            this.groupControl2.Location = new System.Drawing.Point(905, 215);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(300, 216);
            this.groupControl2.TabIndex = 10;
            this.groupControl2.Text = "Meetings";
            // 
            // xfrmJira
            // 
            this.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1217, 741);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.pbBusy);
            this.Controls.Add(this.lblTotalWage);
            this.Controls.Add(this.gcWork);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "xfrmJira";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Jira - Time Track App";
            this.Load += new System.EventHandler(this.xfrmJira_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tbJiraUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbJiraPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcWork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workLogBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFrom.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteTo.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalWage)).EndInit();
            this.lblTotalWage.ResumeLayout(false);
            this.lblTotalWage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbFilterUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbURL.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbProjects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seIssuesRetrieve.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMeetingTicket.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seMeetingDuration.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teMeetingStartTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBusy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbClients.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePayPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTTUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTTPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seWageHour.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit tbJiraUser;
        private DevExpress.XtraEditors.TextEdit tbJiraPassword;
        private DevExpress.XtraEditors.SimpleButton btJiraLogin;
        private DevExpress.XtraEditors.SimpleButton btGetIssues;
        private DevExpress.XtraGrid.GridControl gcWork;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource workLogBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colIssue;
        private DevExpress.XtraGrid.Columns.GridColumn colStartTime;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeSpentMin;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dteTo;
        private DevExpress.XtraEditors.DateEdit dteFrom;
        private DevExpress.XtraEditors.GroupControl lblTotalWage;
        private DevExpress.XtraEditors.ProgressBarControl pbBusy;
        private System.ComponentModel.BackgroundWorker bwGetJSON;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.TextEdit tbTTUser;
        private DevExpress.XtraEditors.TextEdit tbTTPassword;
        private DevExpress.XtraEditors.SimpleButton btTTLogin;
        private DevExpress.XtraEditors.SimpleButton btPostTimeTrack;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LookUpEdit luePayPeriod;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SpinEdit seMeetingDuration;
        private DevExpress.XtraEditors.TimeEdit teMeetingStartTime;
        private DevExpress.XtraEditors.SimpleButton btPopulate;
        private DevExpress.XtraEditors.LabelControl LabelControl100;
        private DevExpress.XtraEditors.SpinEdit seIssuesRetrieve;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.ComboBoxEdit cbClients;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.CheckedListBoxControl lbProjects;
        private DevExpress.XtraEditors.TextEdit tbMeetingTicket;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit tbDescription;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit tbURL;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit tbFilterUser;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl lblTotalHoursPeriod;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl lblHoursThrough;
        private DevExpress.XtraEditors.LabelControl lblHoursLogged;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.SpinEdit seWageHour;
        private DevExpress.XtraEditors.LabelControl lblTotalWagePeriod;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl lblCurrentIncome;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.GroupControl groupControl2;
    }
}