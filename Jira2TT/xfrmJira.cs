﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Net;
using System.Web;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using HtmlAgilityPack;

namespace Jira2TT
{
    public partial class xfrmJira : DevExpress.XtraEditors.XtraForm
    {
        private CookieContainer _jiracookies = null;
        private CookieContainer _ttcookies = null;
        
        private string _ttviewstate = string.Empty;
        private string _tteventvalidation = string.Empty;

        private string _url { get { return tbURL.Text; } }

        List<WorkLog> _workLogs = new List<WorkLog>();
        public xfrmJira()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            InitializeComponent();
            
            dteFrom.DateTime = DateTime.Today.Add(-TimeSpan.FromDays(DateTime.Today.DayOfWeek - DayOfWeek.Monday));
            dteTo.DateTime = DateTime.Now;

            HtmlNode.ElementsFlags.Remove("option");
            teMeetingStartTime.Time = DateTime.Today.AddHours(9).AddMinutes(30);
            cbClients.SelectedIndex = 0;
        }

        private void btJiraLogin_Click(object sender, EventArgs e)
        {
            
            if(_jiracookies == null) _jiracookies = new CookieContainer();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_url);
            request.CookieContainer = _jiracookies;
            request.Method = "GET";
            request.GetResponse();
            //request = (HttpWebRequest)WebRequest.Create(string.Format("http://falcon.trieagleenergy.com/rest/gadget/1.0/login?os_username={0}&os_password={1}&os_captcha=", tbJiraUser.Text, tbJiraPassword.Text));
            request = (HttpWebRequest)WebRequest.Create(GetUri(_url, 
                                                        "/rest/gadget/1.0/login",
                                                        string.Format("os_username={0}&os_password={1}&os_captcha=", tbJiraUser.Text, tbJiraPassword.Text))
                                                        );
            
            request.CookieContainer = _jiracookies;
            request.ContentLength = 0;
            request.Method = "POST";

            HttpWebResponse response = (HttpWebResponse) request.GetResponse();

            StreamReader sr = new StreamReader(response.GetResponseStream());
            string rsp = sr.ReadToEnd();

            var json = (JObject)JsonConvert.DeserializeObject(rsp);

            if (json.GetValue("loginSucceeded").Value<string>() != "True")
            {
                XtraMessageBox.Show("No se pudo autenticar.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _jiracookies = null;
            }
            else
            {
                //Get the list of projects.
                string strJSON = Request(GetUri(_url, "rest/api/latest/project/", string.Empty), string.Empty, "GET", true);
                var jProjects = (JArray)JsonConvert.DeserializeObject(strJSON);

                this.lbProjects.DataSource = jProjects;

                teMeetingStartTime.Enabled = true;
                seMeetingDuration.Enabled = true;
                tbMeetingTicket.Enabled = true;
                btPopulate.Enabled = true; 

                btGetIssues.Enabled = true;
                tbFilterUser.Text = tbJiraUser.Text;
                Properties.Settings.Default.JiraURL = tbURL.Text;
                Properties.Settings.Default.Save();
                tbURL.Enabled = false;
            }
        }

        private void btGetIssues_Click(object sender, EventArgs e)
        {
            if (lbProjects.CheckedItems.Count == 0)
            {
                XtraMessageBox.Show("Please select at least one Jira Project.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            _workLogs.Clear();
            this.lbProjects.Enabled = btGetIssues.Enabled = false;
            bwGetJSON.RunWorkerAsync(new int[]{int.Parse(seIssuesRetrieve.Value.ToString()), lbProjects.CheckedItems.Count});
            
        }

        private void bwGetJSON_DoWork(object sender, DoWorkEventArgs e)
        {
            int intPercentage = 0;
            int intIssuerCnt = 0;
            int intCurrent = 0;
            var _args = e.Argument as int[];

            foreach (var project in this.lbProjects.CheckedItems)
            {
                var jProject = (JObject)JsonConvert.DeserializeObject(project.ToString());

                string strJSON =
                    Request( GetUri(_url, 
                                    "/rest/api/latest/search", 
                                    "jql=project=" + jProject.SelectToken("key") + "&maxResults=" + _args[0].ToString()
                                   ), 
                              string.Empty, "GET", true);

                var jIssues = (JObject) JsonConvert.DeserializeObject(strJSON);

                intIssuerCnt = jIssues.SelectToken("issues").Count();
                foreach (var jIssue in jIssues.SelectToken("issues"))
                {
                    intCurrent++;
                    strJSON = string.Empty;

                    intPercentage = Convert.ToInt32(((double) intCurrent/intIssuerCnt)*(100/_args[1]));
                    bwGetJSON.ReportProgress(intPercentage);
                    strJSON = Request(jIssue.SelectToken("self").ToString(), string.Empty, "GET", true);

                    var jWorkLog = (JObject) JsonConvert.DeserializeObject(strJSON);
                    JToken values = null;
                    
                    values = jWorkLog.SelectToken("fields").SelectToken("worklog").SelectToken("value");
                        
                    if(values == null)
                        values = jWorkLog.SelectToken("fields").SelectToken("worklog").SelectToken("worklogs");


                    
                    if (values != null)
                    {
                        var work =
                            values.Where(x => x.SelectToken("author").SelectToken("name").ToString() == tbFilterUser.Text);
                        DateTime dtTemp;
                        Int64 intTemp;
                        foreach (var item in work)
                        {
                            DateTime.TryParse(item.SelectToken("started").ToString(), out dtTemp);
                            if (!Int64.TryParse((item.SelectToken("minutesSpent") ?? string.Empty).ToString(), out intTemp))
                                if (Int64.TryParse((item.SelectToken("timeSpentSeconds") ?? string.Empty).ToString(), out intTemp))
                                    intTemp = intTemp / 60;

                            _workLogs.Add(new WorkLog(jIssue.SelectToken("key").ToString(), dtTemp, intTemp,
                                                      item.SelectToken("comment").ToString()));
                        }
                    }
                }
            }
        }

        private void bwGetJSON_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            pbBusy.EditValue = 0;
            RefreshData();
            lbProjects.Enabled = btGetIssues.Enabled = true;
        }

        private void bwGetJSON_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pbBusy.EditValue = e.ProgressPercentage;
        }

        private void btTTLogin_Click(object sender, EventArgs e)
        {
            if (_ttcookies == null) _ttcookies = new CookieContainer();
            Request("http://www.prosoftconsulting.net/timetrack/Login.aspx", string.Empty, "GET", false);
            string strHTML = Request("http://www.prosoftconsulting.net/timetrack/Login.aspx", string.Format("&txtUsername={0}&txtPassword={1}&btnLogin=Login", tbTTUser.Text, tbTTPassword.Text), "POST", false);
            var htmlDoc = new HtmlAgilityPack.HtmlDocument();

            htmlDoc.LoadHtml(strHTML);
            
            var payPeriods =
                htmlDoc.DocumentNode.Descendants("select").FirstOrDefault(x => x.Attributes["name"].Value == "ddlPayPeriods");

            if (payPeriods != null)
            {
                var optionspp = from opt in payPeriods.Descendants("option")
                                select new
                                           {
                                               DisplayMember = opt.InnerText,
                                               ValueMember = opt.Attributes["value"].Value
                                           };

                luePayPeriod.Properties.DataSource = optionspp;
                luePayPeriod.ItemIndex = 0;
                btPostTimeTrack.Enabled = true;
            }          

        }

        private void GetVSEV(string strHTML)
        {
            _ttviewstate = string.Empty;
            _tteventvalidation = string.Empty;
 
            strHTML = strHTML.Substring(strHTML.IndexOf("VIEWSTATE"));
            strHTML = strHTML.Substring(strHTML.IndexOf("value=\"") + 7);
            _ttviewstate = Uri.EscapeDataString(strHTML.Substring(0, strHTML.IndexOf("\"")));

            strHTML = strHTML.Substring(strHTML.IndexOf("EVENTVALIDATION"));
            strHTML = strHTML.Substring(strHTML.IndexOf("value=\"") + 7);
            _tteventvalidation = Uri.EscapeDataString(strHTML.Substring(0, strHTML.IndexOf("\"")));
        }

        private string Request(Uri Url, string strCmd, string strMethod, bool Jira)
        {
            return Request(Url.ToString(), strCmd, strMethod, Jira);
        }

        private string Request(string strURL, string strCmd, string strMethod, bool Jira)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(strURL);
            request.CookieContainer = Jira ? _jiracookies : _ttcookies;
            request.Method = strMethod;
            if (strMethod == "POST" && !Jira)
            {
                request.ContentType = "application/x-www-form-urlencoded";
                using (StreamWriter sw = new StreamWriter(request.GetRequestStream()))
                {
                    sw.Write(string.Format("__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE={0}&__EVENTVALIDATION={1}{2}",
                                           _ttviewstate, _tteventvalidation, strCmd));
                }
            }
            else if (strMethod == "POST" && Jira)
            {
                request.ContentType = "application/x-www-form-urlencoded";
                using (StreamWriter sw = new StreamWriter(request.GetRequestStream()))
                {
                    sw.Write(strCmd);
                }
            }
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            string strHTML = string.Empty;
            using(StreamReader sr = new StreamReader(response.GetResponseStream()))
            {
                strHTML = sr.ReadToEnd();
            }
            if(!Jira)
                GetVSEV(strHTML);

            return strHTML;
        }

        private void btPostTimeTrack_Click(object sender, EventArgs e)
        {
            string strHTML = string.Empty;
            string strCMD = string.Empty;
            string strTemp = string.Empty;

            Properties.Settings.Default.Client = cbClients.SelectedItem.ToString();
            Properties.Settings.Default.Save();

            var workLogs = _workLogs.Where(x => x.StartTime >= dteFrom.DateTime && x.StartTime <= dteTo.DateTime).OrderBy(x => x.StartTime);
            foreach (var workLog in workLogs)
            {

                strTemp = strCMD = strHTML = string.Empty;
                Request("http://www.prosoftconsulting.net/timetrack/Default.aspx",
                                  string.Format(
                                      "&ddlPayPeriods={0}&btnChange=View&txtTotalOther=0&intime1=&intime2=&outtime1=&outtime2=&addNewLine=No",
                                      luePayPeriod.EditValue.ToString()),
                                  "POST",
                                  false);
                strHTML = Request("http://www.prosoftconsulting.net/timetrack/Default.aspx",
                                  string.Format(
                                      "&ddlPayPeriods={0}&txtTotalOther=0&btnAdd=Add+New+Entry&intime1=&intime2=&outtime1=&outtime2=&addNewLine=No",
                                      luePayPeriod.EditValue.ToString()),
                                  "POST",
                                  false);

                var htmlDoc = new HtmlAgilityPack.HtmlDocument();

                htmlDoc.LoadHtml(strHTML);

                var inputs =
                    htmlDoc.DocumentNode.Descendants("input").Where(x => x.Attributes["type"].Value == "text");

                List<string> objHTML = inputs.Select(input => input.Attributes["name"].Value).ToList();

                htmlDoc.LoadHtml(strHTML);

                var clients =
                    htmlDoc.DocumentNode.Descendants("select").FirstOrDefault(x => x.Attributes["name"].Value.Contains("ddlClients"));

                if(clients != null)
                    objHTML.Add(clients.Attributes["name"].Value);

                

                strCMD = "&txtTotalOther=0";
                objHTML.ForEach(x =>
                                    {
                                        strTemp = string.Empty;
                                        strTemp =
                                            workLog.Description.Substring(0, workLog.Description.Length >= 50 ? 50 : workLog.Description.Length).Replace(" ", "+");
                                        if (x.Contains("txtItemDate"))
                                            strCMD += string.Format("{0}={1}", Uri.EscapeDataString(x), Uri.EscapeDataString(workLog.StartTime.ToString("MM/dd/yyyy")));
                                        else if (x.Contains("txtProjectNumber"))
                                            strCMD += string.Format("{0}={1}", Uri.EscapeDataString(x), workLog.Issue);
                                        else if (x.Contains("ddlClients"))
                                            strCMD += string.Format("{0}={1}", Uri.EscapeDataString(x), GetClientID(cbClients.SelectedItem.ToString()).ToString());
                                        else if (x.Contains("txtDescription"))
                                            strCMD += string.Format("{0}={1}", Uri.EscapeDataString(x), strTemp);
                                        else if (x.Contains("txtInTime1"))
                                            strCMD += string.Format("{0}={1}", Uri.EscapeDataString(x), Uri.EscapeDataString(workLog.StartTime.ToString("HH:mm")));
                                        else if (x.Contains("txtOutTime1"))
                                            strCMD += string.Format("{0}={1}", Uri.EscapeDataString(x), Uri.EscapeDataString(workLog.StartTime.AddMinutes(workLog.TimeSpentMin).ToString("HH:mm")));
                                        else if (x.Contains("txtInTime2"))
                                            strCMD += string.Format("{0}={1}", Uri.EscapeDataString(x), Uri.EscapeDataString("00:00"));
                                        else if (x.Contains("txtOutTime2"))
                                            strCMD += string.Format("{0}={1}", Uri.EscapeDataString(x), Uri.EscapeDataString("00:00"));
                                        else if (x.Contains("txtTotalHours"))
                                            strCMD += string.Format("{0}={1}", Uri.EscapeDataString(x), "0.0");
                                        
                                        strCMD += "&";
                                    });

                var save =
                    htmlDoc.DocumentNode.Descendants("input").FirstOrDefault(x => x.Attributes["type"].Value == "submit" && x.Attributes["value"].Value == "Save");

                if (save != null)
                    strCMD += string.Format("{0}={1}", Uri.EscapeDataString(save.Attributes["name"].Value), save.Attributes["value"].Value);


                strCMD += string.Format("&intime1={0}&intime2={1}&outtime1={2}&outtime2={3}&addNewLine=Yes",
                                        Uri.EscapeDataString(workLog.StartTime.ToString("HH:mm")),
                                        Uri.EscapeDataString(workLog.StartTime.AddMinutes(workLog.TimeSpentMin).ToString("HH:mm")), 
                                        Uri.EscapeDataString("00:00"), Uri.EscapeDataString("00:00"));
                Request("http://www.prosoftconsulting.net/timetrack/Default.aspx", strCMD, "POST", false);

            }
        }

        private void cePopulateSCRUM_CheckedChanged(object sender, EventArgs e)
        {
            /*
            if (cePopulateSCRUM.Checked)
            {
                teMeetingStartTime.Enabled = true;
                seMeetingDuration.Enabled = true;
                btPopulate.Enabled = true;             
            }
            else
            {
                teMeetingStartTime.Enabled = false;
                seMeetingDuration.Enabled = false;
                btPopulate.Enabled = false;
                var torm = _workLogs.Where(x => x.Description.Contains("Scrum Meeting")).ToList();
                for (int i = 0; i < torm.Count; i++)
                {
                    _workLogs.Remove(torm[i]);
                }
            }
            RefreshData();
             */
        }

        private void RefreshData()
        {
            gcWork.DataSource = _workLogs.Where(x => x.StartTime >= dteFrom.DateTime && x.StartTime <= dteTo.DateTime);
            lblHoursLogged.Text = (_workLogs.Where(x => x.StartTime >= dteFrom.DateTime && x.StartTime <= dteTo.DateTime).Sum(x => x.TimeSpentMin) / 60.0).ToString();
        }

        private void btPopulate_Click(object sender, EventArgs e)
        {
            if (tbMeetingTicket.Text == string.Empty)
            {
                XtraMessageBox.Show("Please write a valid ticket number.", "Error", MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                return;
            }

            try
            {
                DateTime dteTemp = dteFrom.DateTime;
                while (dteTemp <= dteTo.DateTime)
                {
                    StringBuilder sb = new StringBuilder();
                    StringWriter sw = new StringWriter(sb);
 
                    using (JsonWriter writer = new JsonTextWriter(sw))
                    {
                        writer.Formatting = Formatting.Indented;
 
                        writer.WriteStartObject();
                        writer.WritePropertyName("comment");
                        writer.WriteValue(tbDescription.Text);
                        writer.WritePropertyName("started");
                        writer.WriteValue(dteTemp.AddHours(teMeetingStartTime.Time.Hour).AddMinutes(teMeetingStartTime.Time.Minute));
                        writer.WritePropertyName("timeSpentSeconds");
                        writer.WriteValue(seMeetingDuration.Value * 60);
                        writer.WriteEndObject();
                    }

                    if (dteTemp.DayOfWeek != DayOfWeek.Saturday && dteTemp.DayOfWeek != DayOfWeek.Sunday)
                        Request(GetUri(_url, string.Format("/rest/api/latest/issue/{0}/worklog/", tbMeetingTicket.Text), string.Empty),
                                sb.ToString(), "POST", true);

                    dteTemp = dteTemp.AddDays(1);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("There was an exception while trying to log work into Jira.", "Error", MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
            }
            
            /*
            --Previous usage was to populate meetings in the list obtained from Jira.
            DateTime dteTemp = dteFrom.DateTime;
            while (dteTemp <= dteTo.DateTime)
            {
                if(dteTemp.DayOfWeek != DayOfWeek.Saturday && dteTemp.DayOfWeek != DayOfWeek.Sunday)
                    _workLogs.Add(new WorkLog(string.Empty, dteTemp.AddHours(teMeetingStartTime.Time.Hour).AddMinutes(teMeetingStartTime.Time.Minute), Convert.ToInt64(seMeetingDuration.EditValue), "Scrum Meeting"));
                dteTemp = dteTemp.AddDays(1);
            }
            RefreshData();
            */
        }

        private void xfrmJira_Load(object sender, EventArgs e)
        {
            tbURL.Text = Properties.Settings.Default.JiraURL;
            cbClients.SelectedItem = Properties.Settings.Default.Client;

            int _totalhours = 0;
            int _currenthours = 0;

            int _start = 0, _end = 0;
            
            _start = DateTime.Today.Day <= 15 ? 1 : 16;
            _end = DateTime.Today.Day <= 15 ? 15 : DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month);
            
            for (int i = _start; i <= _end; i++)
            {
                var date = new DateTime(DateTime.Today.Year, DateTime.Today.Month, i);
                if (date.DayOfWeek != DayOfWeek.Saturday && date.DayOfWeek != DayOfWeek.Sunday)
                {
                    _totalhours += 8;
                    if (date <= DateTime.Today)
                        _currenthours += 8;
                }
            }

            lblTotalHoursPeriod.Text = _totalhours.ToString();
            lblHoursThrough.Text = _currenthours.ToString();

        }

        private Uri GetUri(string Url, string Path, string Query)
        {
            UriBuilder uriBuilder = new UriBuilder(Url)
                                        {
                                            Path = Path,
                                            Query = Query,                                            
                                        };

            return uriBuilder.Uri;
        }
        private Uri GetUri(string Url, string Path, string Query, string User, string Password)
        {
            UriBuilder uriBuilder = new UriBuilder(Url)
                                        {
                                            Path = Path,
                                            Query = Query,
                                            Password = Password,
                                            UserName = User
                                        };
            return uriBuilder.Uri;
        }

        private void pbBusy_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
        {
            e.DisplayText = "Hello";
        }

        private void seWageHour_EditValueChanged(object sender, EventArgs e)
        {
            lblCurrentIncome.Text = (Convert.ToDouble(lblHoursLogged.Text) * Convert.ToDouble(seWageHour.EditValue)).ToString();
            lblTotalWagePeriod.Text = (Convert.ToDouble(lblTotalHoursPeriod.Text) * Convert.ToDouble(seWageHour.EditValue)).ToString();
        }

        private int GetClientID(string Name)
        {
            switch (Name)
            {
                case "One Legal":
                    return 191;
                case "Trieagle":
                    return 178;
                default:
                    return 1;
            }
        }

    }
}