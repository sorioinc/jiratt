﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jira2TT
{
    class WorkLog
    {
        public string Issue { get; set; }
        public DateTime StartTime { get; set; }
        public Int64 TimeSpentMin { get; set; }
        public string Description { get; set; }
        

        public WorkLog(string Issue, DateTime StartTime, Int64 TimeSpentMin, string Description)
        {
            this.Issue = Issue;
            this.StartTime = StartTime;
            this.TimeSpentMin = TimeSpentMin;
            this.Description = Description;
        }
    }
}
